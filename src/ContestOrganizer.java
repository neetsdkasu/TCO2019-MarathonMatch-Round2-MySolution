import java.io.*;
import java.util.*;

public class ContestOrganizer {
    static Random rand = new Random(19831983L);
    Stopwatch sw = new Stopwatch();

    int N, M, C;
    int[][] wins;
    int[] sumwins, swIndexes;

    public String[] makeTeams(int M, String[] sWins) {
        sw.start();
        this.M = M;
        this.N = sWins.length;
        wins = new int[N][N];
        sumwins = new int[N];
        int minWins = Integer.MAX_VALUE, maxWins = 0;
        for (int  i = 0; i < N; i++) {
            String[] vs = sWins[i].split(" ");
            for (int j = 0; j < N; j++) {
                wins[i][j] = Integer.parseInt(vs[j]);
                sumwins[i] += wins[i][j];
            }
            minWins = Math.min(minWins, sumwins[i]);
            maxWins = Math.max(maxWins, sumwins[i]);
        }
        for (int  i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                C = Math.max(C, wins[i][j] + wins[j][i]);
            }
        }
        System.err.println("N: " + N);
        System.err.println("M: " + M);
        System.err.println("C: " + C);

        int[] skills = new int[N];
        for (int i = 0; i < N; i++) {
            skills[i] = 900 * (sumwins[i] - minWins) / (maxWins - minWins) + 100;
        }
        for (int i = 0; i < 5; i++) {
            if (!fit(skills)) {
                System.err.println("stoped: " + i);
                break;
            }
        }

        swIndexes = new int[N];
        for (int i = 0; i < N; i++) {
            swIndexes[i] = skills[i] * N + i;
        }
        Arrays.sort(swIndexes);
        for (int i = 0; i < N; i++) {
            swIndexes[i] %= N;
        }

        Tuple<Double, IntList[]> teams = makeTeamNumOrder(skills), tmpTeams;
        System.err.println("sc: " + teams.item1);

        tmpTeams = makeTeamIncOrder(skills);
        System.err.println("sc: " + tmpTeams.item1);
        if (tmpTeams.item1 < teams.item1) { teams = tmpTeams; }

        tmpTeams = makeTeamDecOrder(skills);
        System.err.println("sc: " + tmpTeams.item1);
        if (tmpTeams.item1 < teams.item1) { teams = tmpTeams; }

        tmpTeams = makeTeamIncDecSwitch(skills);
        System.err.println("sc: " + tmpTeams.item1);
        if (tmpTeams.item1 < teams.item1) { teams = tmpTeams; }

        tmpTeams = makeTeamDecIncSwitch(skills);
        System.err.println("sc: " + tmpTeams.item1);
        if (tmpTeams.item1 < teams.item1) { teams = tmpTeams; }

        return Arrays.stream(teams.item2).map(IntList::toString).toArray(String[]::new);
    }

    int[] makeSkillSet() {
        int[] skills = new int[N];
        int[] tmp = new int[N];
        for (int i = 0; i < N; i++) {
            tmp[i] = rand.nextInt(901) + 100;
        }
        Arrays.sort(tmp);
        for (int i = 0; i < N; i++) {
            skills[swIndexes[i]] = tmp[i];
        }
        return skills;
    }

    int simContests(int[] skills) {
        final int[][] tmpWins = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                tmpWins[i][j] = C * skills[i] / (skills[i] + skills[j]);
            }
        }
        int score = 0;
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                int d = tmpWins[i][j] - wins[i][j];
                score += d * d;
            }
        }
        return score;
    }

    boolean fit(int[] skills, int inc) {
        boolean upd = false;
        for (int i = 0; i < N; i++) {
            int bestS = skills[i];
            int minDiff = Integer.MAX_VALUE;
            for (int s = 100; s <= 1000; s += inc) {
                int diff = 0;
                for (int j = 0; j < N; j++) {
                    if (i == j) { continue; }
                    int w = C * s / (s + skills[j]);
                    int d = w - wins[i][j];
                    diff += d * d;
                }
                if (diff < minDiff) {
                    bestS = s;
                    minDiff = diff;
                }
            }
            upd |= skills[i] != bestS;
            skills[i] = bestS;
        }
        return upd;
    }

    boolean fit(int[] skills) {
        boolean upd = false;
        for (int i = 0; i < N; i++) {
            int bestS = skills[i];
            int minDiff = Integer.MAX_VALUE;
            int minS = Math.max(100, bestS - 50);
            int maxS = Math.min(1000, bestS + 50);
            for (int s = minS; s <= maxS; s++) {
                int diff = 0;
                for (int j = 0; j < N; j++) {
                    if (i == j) { continue; }
                    int w = C * s / (s + skills[j]);
                    int d = w - wins[i][j];
                    diff += d * d;
                }
                if (diff < minDiff) {
                    bestS = s;
                    minDiff = diff;
                }
            }
            upd |= skills[i] != bestS;
            skills[i] = bestS;
        }
        return upd;
    }

    IntList[] getEmptyTeams() {
        IntList[] teams = new IntList[M];
        for (int i = 0; i < teams.length; i++) {
            teams[i] = new IntList();
        }
        return teams;
    }

    double calcScore(int[] skills, IntList[] teams) {
        double min = Double.MAX_VALUE;
        double max = 0;
        for (IntList team : teams) {
            int sum = 0;
            for (int i = 0; i < team.size(); i++) {
                sum += (i + 1) * skills[team.get(i)];
            }
            sum *= 2;
            double strength = (double)sum / (double)(team.size() * (team.size() + 1));
            min = Math.min(min, strength);
            max = Math.max(max, strength);
        }
        return max - min;
    }

    Tuple<Double, IntList[]> makeTeamNumOrder(int[] skills) {
        IntList[] teams = getEmptyTeams();
        for (int i = 0; i < N; i++) {
            teams[i % M].add(i);
        }
        double score = calcScore(skills, teams);
        return new Tuple<>(score, teams);
    }

    Tuple<Double, IntList[]> makeTeamIncOrder(int[] skills) {
        IntList[] teams = getEmptyTeams();
        for (int i = 0; i < N; i++) {
            teams[i % M].add(swIndexes[i]);
        }
        double score = calcScore(skills, teams);
        return new Tuple<>(score, teams);
    }

    Tuple<Double, IntList[]> makeTeamDecOrder(int[] skills) {
        IntList[] teams = getEmptyTeams();
        for (int i = 0; i < N; i++) {
            teams[i % M].add(swIndexes[N - 1 - i]);
        }
        double score = calcScore(skills, teams);
        return new Tuple<>(score, teams);
    }

    Tuple<Double, IntList[]> makeTeamIncDecSwitch(int[] skills) {
        IntList[] teams = getEmptyTeams();
        int p1 = 0, p2 = N - 1;
        for (int i = 0; i < N; i++) {
            if ((i / M) % 2 == 0) {
                teams[i % M].add(swIndexes[p1]);
                p1++;
            } else {
                teams[i % M].add(swIndexes[p2]);
                p2--;
            }
        }
        double score = calcScore(skills, teams);
        return new Tuple<>(score, teams);
    }

    Tuple<Double, IntList[]> makeTeamDecIncSwitch(int[] skills) {
        IntList[] teams = getEmptyTeams();
        int p1 = 0, p2 = N - 1;
        for (int i = 0; i < N; i++) {
            if ((i / M) % 2 != 0) {
                teams[i % M].add(swIndexes[p1]);
                p1++;
            } else {
                teams[i % M].add(swIndexes[p2]);
                p2--;
            }
        }
        double score = calcScore(skills, teams);
        return new Tuple<>(score, teams);
    }

    public static void main(String[] args) throws Exception {
        Main.main(args);
    }
}

class Tuple<U, V> {
    public final U item1;
    public final V item2;
    public Tuple(U i1, V i2) {
        item1 = i1;
        item2 = i2;
    }
}

class IntList {
    int[] buf;
    int index;
    final int growup;
    public IntList() {
        this(10, 10);
    }
    public IntList(int capa, int g) {
        buf = new int[capa];
        index = 0;
        growup = g;
    }
    public int size() {
        return index;
    }
    public void add(int v) {
        if (index == buf.length) {
            buf = Arrays.copyOf(buf, buf.length + growup);
        }
        buf[index] = v;
        index++;
    }
    public int get(int i) {
        return buf[i];
    }
    public void set(int i, int v) {
        buf[i] = v;
    }
    public void remove(int i) {
        index--;
        for (int d = i; d < index; d++) {
            buf[d] = buf[d + 1];
        }
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(buf[0]);
        for (int i = 1; i < index; i++) {
            sb.append(' ').append(buf[i]);
        }
        return sb.toString();
    }
}

class Main {

    static int callMakeTeams(BufferedReader in, ContestOrganizer contestOrganizer) throws Exception {
        Stopwatch sw = new Stopwatch();

        int M = Integer.parseInt(in.readLine());
        int _winsSize = Integer.parseInt(in.readLine());
        String[] wins = new String[_winsSize];
        for (int _idx = 0; _idx < _winsSize; _idx++) {
            wins[_idx] = in.readLine();
        }

        sw.start();
        String[] _result = contestOrganizer.makeTeams(M, wins);
        sw.stop();
        System.err.printf("time: %d ms%n", sw.getTotalTime());

        System.out.println(_result.length);
        for (String _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        ContestOrganizer contestOrganizer = new ContestOrganizer();

        callMakeTeams(in, contestOrganizer);
    }
}

class Stopwatch {
    long startTime = 0L;
    long totalTime = 0L;
    public void reset() {
        startTime = 0;
        totalTime = 0;
    }
    public void start() {
        startTime = System.currentTimeMillis();
    }
    public long stop() {
        long stopTime = System.currentTimeMillis();
        long interval = stopTime - startTime;
        totalTime += interval;
        return interval;
    }
    public long nowInterval() {
        long stopTime = System.currentTimeMillis();
        long interval = stopTime - startTime;
        return interval;
    }
    public long nowTotal() {
        long stopTime = System.currentTimeMillis();
        long interval = stopTime - startTime;
        return totalTime + interval;
    }
    public long getTotalTime() {
        return totalTime;
    }
}
