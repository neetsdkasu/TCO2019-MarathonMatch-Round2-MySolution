TCO2019 Marathon Match Round 2 My Solution
==========================================


Challenge page   
https://www.topcoder.com/challenges/30101930  


Standings  
https://www.topcoder.com/challenges/30101930?tab=submissions  


Forum  
https://apps.topcoder.com/forums/?module=Category&categoryID=78094  


Review page (old platform?)  
https://software.topcoder.com/review/actions/ViewProjectDetails?pid=30101930   


Review page (new platform)  
https://submission-review.topcoder.com/challenges/30101930   

