@ECHO OFF
IF NOT "%~2"=="" GOTO label2args
IF NOT "%~1"=="" GOTO label1arg
ECHO Testset seed 1 to 10
FOR /L %%I IN (1,1,10) DO @(
    @ECHO ________________________________________________________________
    @ECHO Seed ... %%I & @a.bat %%I -novis
    @if ERRORLEVEL 1 goto finally
)
GOTO finally
:label1arg
ECHO Testset seed 1 to %1
FOR /L %%I IN (1,1,%1) DO @(
    @ECHO ________________________________________________________________
    @ECHO Seed ... %%I & @a.bat %%I -novis
    @if ERRORLEVEL 1 goto finally
)
GOTO finally
:label2args
ECHO Testset seed %1 to %2
FOR /L %%I IN (%1,1,%2) DO @(
    @ECHO ________________________________________________________________
    @ECHO Seed ... %%I & @a.bat %%I %3 %4 %5 %6 %7 %8 %9 -novis
    @if ERRORLEVEL 1 goto finally
)
GOTO finally
:finally
