@echo off
@setlocal
@set MYCMD=java -cp classes ContestOrganizer
if "%~1"=="" goto defaultseed
java -jar tester.jar -exec "%MYCMD% -seed %*" -seed %*
@GOTO finally
:defaultseed
java -jar tester.jar -exec "%MYCMD% -seed 1" -seed 1
@GOTO finally
:finally
@endlocal